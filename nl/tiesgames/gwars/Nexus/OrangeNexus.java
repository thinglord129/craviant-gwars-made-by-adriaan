package nl.tiesgames.gwars.Nexus;

import nl.tiesgames.gwars.Golems.OrangeGolem;
import nl.tiesgames.gwars.Managers.MessageManager;
import nl.tiesgames.gwars.Teams.Teams;
import nl.tiesgames.gwars.enums.Nexus;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class OrangeNexus implements Listener {

	int radius = 2;
	int i = 1;

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {

		Player p = e.getPlayer();
		Block b = e.getBlock();
		Location loc = b.getLocation();
		
		if (e.getBlock().getType() == Material.STAINED_CLAY
				&& e.getBlock().getData() == (byte) 1) {
		World world = loc.getWorld();

		if(Teams.orange.hasPlayer(p)) {

			e.setCancelled(true);
			p.sendMessage(ChatColor.GOLD + "You cant damage your own core!");
			
		} else {

			if (i != 50) {
				MessageManager.nexusDamage(Nexus.ORANGE, p);

				loc.getWorld().playSound(loc, Sound.ANVIL_LAND, 10.0F, 1.0F);
				loc.getWorld().playEffect(loc, Effect.FLAME, 100);
				loc.getWorld().playEffect(loc, Effect.FLAME, 100);
				loc.getWorld().playEffect(loc, Effect.FLAME, 100);
				loc.getWorld().playEffect(loc, Effect.FLAME, 100);
				loc.getWorld().playEffect(loc, Effect.FLAME, 100);
				loc.getWorld().playEffect(loc, Effect.FLAME, 100);
				loc.getWorld().playEffect(loc, Effect.FLAME, 100);
				loc.getWorld().playEffect(loc, Effect.FLAME, 100);
				loc.getWorld().playEffect(loc, Effect.FLAME, 100);
				loc.getWorld().playEffect(loc, Effect.FLAME, 100);

				i++;

				e.setCancelled(true);

			}

			else if (i == 50) {

				MessageManager.nexusDestroy(Nexus.ORANGE, p);
				loc.getWorld().playSound(loc, Sound.ANVIL_BREAK, 10.0F, 1.0F); 
				loc.getWorld().playSound(loc, Sound.EXPLODE, 10.0F, 1.0F);
				loc.getWorld().playEffect(loc, Effect.EXPLOSION_HUGE, 100);
				loc.getWorld().playEffect(loc, Effect.EXPLOSION_HUGE, 100);
				loc.getWorld().playEffect(loc, Effect.EXPLOSION_HUGE, 100);
				loc.getWorld().playEffect(loc, Effect.EXPLOSION_HUGE, 100);
				loc.getWorld().playEffect(loc, Effect.EXPLOSION_HUGE, 100);
				loc.getWorld().playEffect(loc, Effect.EXPLOSION_HUGE, 100);
				loc.getWorld().playEffect(loc, Effect.EXPLOSION_HUGE, 100);
				loc.getWorld().playEffect(loc, Effect.EXPLOSION_HUGE, 100);
				loc.getWorld().playEffect(loc, Effect.EXPLOSION_HUGE, 100);
				loc.getWorld().playEffect(loc, Effect.EXPLOSION_HUGE, 100);
				
				OrangeGolem.spawn();

				b.setType(Material.AIR);

				e.setCancelled(true);
				
				for (int x = -radius; x < radius; x++) {
	                for (int y = -radius; y < radius; y++) {
	                    for (int z = -radius; z < radius; z++) {
	 
	                        Block remove = world.getBlockAt(loc.getBlockX() + x,
	                                loc.getBlockY() + y, loc.getBlockZ() + z);
	 
	                        if (remove.getType().equals(Material.BRICK)) {
	                            remove.setType(Material.AIR);

							}

						}

					}

				}

			}

		}

	}

	}
	
	}
	


	

