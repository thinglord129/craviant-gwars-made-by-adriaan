package nl.tiesgames.gwars.BlockRespawn;

import nl.tiesgames.gwars.Main;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class Oak implements Listener {

	@EventHandler
	public void onPlayerOak(BlockBreakEvent e) {

		final Block b = e.getBlock();
		Player p = e.getPlayer();

		if (e.getBlock().getType().equals(Material.LOG)) {
			b.setType(Material.AIR);

			ItemStack Log = new ItemStack(Material.LOG);
			p.getInventory().addItem(Log);

			e.setCancelled(true);

			Bukkit.getScheduler().runTaskLater(Main.getMain(), new Runnable() {

				@Override
				public void run() {
					b.setType(Material.LOG);

				}

			}, 20 * 10);

		}

	}

}
