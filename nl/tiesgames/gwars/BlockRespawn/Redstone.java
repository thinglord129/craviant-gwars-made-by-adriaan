package nl.tiesgames.gwars.BlockRespawn;

import nl.tiesgames.gwars.Main;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class Redstone implements Listener {
	
	@EventHandler
	public void onPlayerRedstone(BlockBreakEvent e) {
		
		final Block b = e.getBlock();
		Player p = e.getPlayer();
				
		if(e.getBlock().getType().equals(Material.REDSTONE_ORE)) {
		b.setType(Material.BEDROCK);
		p.giveExp(3);
		
		ItemStack Redstone = new ItemStack(Material.REDSTONE, 16);
	    p.getInventory().addItem(Redstone);
		
		e.setCancelled(true);
		
		Bukkit.getScheduler().runTaskLater(Main.getMain(), new Runnable() {

			@Override
			public void run() {
			b.setType(Material.REDSTONE_ORE);

				
			}
		
		}, 20 * 30);
		
}

}
	
}
