package nl.tiesgames.gwars.BlockRespawn;

import nl.tiesgames.gwars.Main;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class Leaves implements Listener {

@EventHandler
public void onPlayerLeave(BlockBreakEvent e) {
	
	final Block b = e.getBlock();
			
	if(b.getType().equals(Material.LEAVES)) {
	
	Bukkit.getScheduler().runTaskLater(Main.getMain(), new Runnable() {

		@Override
		public void run() {
		b.setType(Material.LEAVES);

		}
	
	}, 20 * 5);{

}
	
	}
	
}

}
