package nl.tiesgames.gwars.BlockRespawn;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class PlayerPlaceBlock implements Listener {

	@EventHandler
	public void onPlayerMelon(BlockPlaceEvent e) {

		Player p = e.getPlayer();
		Block b = e.getBlock();

		GameMode gamemode = p.getGameMode();

		if (b.getType() == Material.MELON_BLOCK) {
			if (gamemode == GameMode.ADVENTURE) {

				e.setCancelled(true);

			}

		}

	}

	@EventHandler
	public void onPlayerWood(BlockPlaceEvent e) {

		Player p = e.getPlayer();
		Block b = e.getBlock();

		GameMode gamemode = p.getGameMode();

		if (b.getType() == Material.LOG) {
			if (gamemode == GameMode.ADVENTURE) {

				e.setCancelled(true);

			}

		}

	}

	@EventHandler
	public void onPlayerIron(BlockPlaceEvent e) {

		Player p = e.getPlayer();
		Block b = e.getBlock();

		GameMode gamemode = p.getGameMode();

		if (b.getType() == Material.IRON_ORE) {
			if (gamemode == GameMode.ADVENTURE) {

				e.setCancelled(true);

			}

		}

	}

	@EventHandler
	public void onPlayerGold(BlockPlaceEvent e) {

		Player p = e.getPlayer();
		Block b = e.getBlock();

		GameMode gamemode = p.getGameMode();

		if (b.getType() == Material.GOLD_ORE) {
			if (gamemode == GameMode.ADVENTURE) {

				e.setCancelled(true);

			}

		}

	}

	@EventHandler
	public void onPlayerLeave(BlockPlaceEvent e) {

		Player p = e.getPlayer();
		Block b = e.getBlock();

		GameMode gamemode = p.getGameMode();
		
			if (b.getType() == Material.LEAVES) {
				if (gamemode == GameMode.ADVENTURE) {

				e.setCancelled(true);

			}

		}

	}

	@EventHandler
	public void onPlayerCobblestone(BlockPlaceEvent e) {

		Player p = e.getPlayer();
		Block b = e.getBlock();

		GameMode gamemode = p.getGameMode();
		
			if (b.getType() == Material.COBBLESTONE) {
				if (gamemode == GameMode.ADVENTURE) {

				e.setCancelled(true);

			}

		}

	}

}
