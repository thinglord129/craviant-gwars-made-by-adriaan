package nl.tiesgames.gwars.BlockRespawn;

import nl.tiesgames.gwars.Main;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class Diamond implements Listener {
	
	@EventHandler
	public void onPlayerDiamond(BlockBreakEvent e) {
		
		final Block b = e.getBlock();
		Player p = e.getPlayer();
				
		if(e.getBlock().getType().equals(Material.DIAMOND_ORE)) {
		b.setType(Material.BEDROCK);
		p.giveExp(3);
		
		ItemStack Diamond = new ItemStack(Material.DIAMOND);
	    p.getInventory().addItem(Diamond);
		
		e.setCancelled(true);
		
		Bukkit.getScheduler().runTaskLater(Main.getMain(), new Runnable() {

			@Override
			public void run() {
			b.setType(Material.DIAMOND_ORE);

				
			}
		
		}, 20 * 30);
		
}

}
	
}
