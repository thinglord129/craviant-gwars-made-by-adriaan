package nl.tiesgames.gwars.KitManager;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

public class VIPWand implements Listener {

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {

		Player p = e.getPlayer();

		if (p.getItemInHand().equals(ItemStacks.coal())) {

			Block b = p.getTargetBlock(null, 50);
			Location loc = b.getLocation();

			try {
				FireworkEffectPlayer.playFirework(loc, FireworkEffect.builder()
						.with(FireworkEffect.Type.BURST).withColor(Color.RED)
						.withFade(Color.BLACK).build());

			} catch (IllegalArgumentException ex) {
				ex.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();

			}

		}

	}

}
