package nl.tiesgames.gwars.KitManager;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class KitManager {
	
	@SuppressWarnings("deprecation")
	public static void BasicKit(Player p) {
		
		p.getInventory().clear();
		
		ItemStack craft = new ItemStack(Material.WORKBENCH);
		ItemStack sword = new ItemStack(Material.WOOD_SWORD);
		ItemStack pickaxe = new ItemStack(Material.WOOD_PICKAXE);
		ItemStack axe = new ItemStack(Material.WOOD_AXE);
		
		p.getInventory().addItem(craft);
		p.getInventory().addItem(sword);
		p.getInventory().addItem(pickaxe);
		p.getInventory().addItem(axe);
		
		p.updateInventory();
		
	}
	
@SuppressWarnings("deprecation")
public static void WandKit(Player p) {
		
		p.getInventory().clear();
		
		ItemStack sword = new ItemStack(Material.WOOD_SWORD);
		ItemStack pickaxe = new ItemStack(Material.WOOD_PICKAXE);
		ItemStack axe = new ItemStack(Material.WOOD_AXE);
		
		p.getInventory().addItem(ItemStacks.coal());
		p.getInventory().addItem(sword);
		p.getInventory().addItem(pickaxe);
		p.getInventory().addItem(axe);
		
		p.updateInventory();

}

}