package nl.tiesgames.gwars.Managers;

import nl.tiesgames.gwars.Game.Game;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

public class ScoreboardManager {

	@SuppressWarnings("deprecation")
	public static void updateScoreboard(Player p) {
		Scoreboard board = p.getScoreboard();
		Objective ob = board.getObjective(DisplaySlot.SIDEBAR);

		ob.setDisplayName(ChatColor.WHITE + "[" + ChatColor.GOLD + "gWars"
				+ ChatColor.WHITE + "]");

		ob.getScore(Bukkit.getOfflinePlayer(ChatColor.GRAY
				+ "Game starts in")).setScore(Game.seconds);

		ob.getScore(Bukkit.getOfflinePlayer(ChatColor.GRAY
				+ "Players")).setScore(Bukkit.getOnlinePlayers().length);
	}

}
