package nl.tiesgames.gwars.Managers;

import java.util.ArrayList;

import nl.tiesgames.gwars.Teams.Teams;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;

public class SignManager {
	
	public static ArrayList<Location> redsigns = new ArrayList<Location>();
	public static ArrayList<Location> cyansigns = new ArrayList<Location>();
	public static ArrayList<Location> yellowsigns = new ArrayList<Location>();
	public static ArrayList<Location> orangesigns = new ArrayList<Location>();
	
	public static void updateRedSigns(){
		for (Location signs : redsigns){
			Block b = signs.getBlock();
			
			if (b.getState() instanceof Sign){
				Sign s = (Sign) b.getState();
				s.setLine(1, ChatColor.WHITE + "Players " + ChatColor.GOLD + Teams.red.getSize());
				s.update(true);
			} else {
				return;
			}
		}
	}
	
	public static void updateCyanSigns(){
		for (Location signs : cyansigns){
			Block b = signs.getBlock();
			
			if (b.getState() instanceof Sign){
				Sign s = (Sign) b.getState();
				s.setLine(1, ChatColor.WHITE + "Players " + ChatColor.GOLD + Teams.cyan.getSize());
				s.update(true);
			} else {
				return;
			}
		}
	}
	
	public static void updateYellowSigns(){
		for (Location signs : yellowsigns){
			Block b = signs.getBlock();
			
			if (b.getState() instanceof Sign){
				Sign s = (Sign) b.getState();
				s.setLine(1, ChatColor.WHITE + "Players " + ChatColor.GOLD + Teams.yellow.getSize());
				s.update(true);
			} else {
				return;
			}
		}
	}
	
	public static void updateOrangeSigns(){
		for (Location signs : orangesigns){
			Block b = signs.getBlock();
			
			if (b.getState() instanceof Sign){
				Sign s = (Sign) b.getState();
				s.setLine(1, ChatColor.WHITE + "Players " + ChatColor.GOLD + Teams.orange.getSize());
				s.update(true);
			} else {
				return;
			}
		}
	}
	
	

}
