package nl.tiesgames.gwars.Managers;

import nl.tiesgames.gwars.enums.Nexus;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class MessageManager {
	
	public static void nexusDamage(Nexus n, Player p){
		Bukkit.broadcastMessage(ChatColor.GOLD + "[" + ChatColor.GRAY + p.getName() + ChatColor.GOLD + "]" + ChatColor.RED + " damaged the " + ChatColor.DARK_AQUA + n.getName() + ChatColor.RED +  " Core!");
	}
	
	public static void nexusDestroy(Nexus n, Player p){
		Bukkit.broadcastMessage(ChatColor.GRAY + "-=-=-=-=- " + ChatColor.GOLD + "Core Destroyed" + ChatColor.GRAY + " -=-=-=-=-");
		Bukkit.broadcastMessage(ChatColor.RED + "  The " + ChatColor.DARK_AQUA + n.getName() + ChatColor.RED + " core is DOWN!");
		Bukkit.broadcastMessage(ChatColor.RED + "  The " + ChatColor.DARK_AQUA + p.getName() + ChatColor.RED + " did the last hit!");
		Bukkit.broadcastMessage(ChatColor.GRAY + "-=-=-=-=- " + ChatColor.GOLD + "Core Destroyed" + ChatColor.GRAY + " -=-=-=-=-");
	}

}
