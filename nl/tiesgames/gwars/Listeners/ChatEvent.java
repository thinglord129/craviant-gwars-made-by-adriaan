package nl.tiesgames.gwars.Listeners;

import nl.tiesgames.gwars.Teams.Teams;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatEvent implements Listener {

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent e) {

		Player p = e.getPlayer();
		String name = p.getName();
		String message = e.getMessage();
		e.setCancelled(true);

		if (Teams.red.hasPlayer(p)) {

			for (Player red : Bukkit.getOnlinePlayers()) {

				if (Teams.red.hasPlayer(red)) {

					red.sendMessage(ChatColor.RED + "RED " + ChatColor.GRAY
							+ name + ChatColor.GRAY + ": " + ChatColor.GRAY
							+ message);

				}

			}

		} else if (Teams.yellow.hasPlayer(p)) {

			for (Player yellow : Bukkit.getOnlinePlayers()) {

				if (Teams.yellow.hasPlayer(yellow)) {

					yellow.sendMessage(ChatColor.YELLOW + "YELLOW "
							+ ChatColor.GRAY + name + ChatColor.GRAY + ": "
							+ ChatColor.GRAY + message);

				}
			}

		} else if (Teams.orange.hasPlayer(p)) {

			for (Player orange : Bukkit.getOnlinePlayers()) {

				if (Teams.orange.hasPlayer(orange)) {

					orange.sendMessage(ChatColor.GOLD + "ORANGE "
							+ ChatColor.GRAY + name + ChatColor.GRAY + ": "
							+ ChatColor.GRAY + message);

				}

			}

		} else if (Teams.cyan.hasPlayer(p)) {

			for (Player cyan : Bukkit.getOnlinePlayers()) {

				if (Teams.cyan.hasPlayer(cyan)) {

					cyan.sendMessage(ChatColor.AQUA + "CYAN " + ChatColor.GRAY
							+ name + ChatColor.GRAY + ": " + ChatColor.GRAY
							+ message);

				}

			}

		} else {
			Bukkit.broadcastMessage(ChatColor.BLUE + "PUBLIC " + ChatColor.GRAY
					+ name + ChatColor.GRAY + ": " + ChatColor.GRAY + message);
		}

	}

}
