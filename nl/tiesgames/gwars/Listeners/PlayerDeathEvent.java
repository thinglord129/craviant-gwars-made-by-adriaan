package nl.tiesgames.gwars.Listeners;

import nl.tiesgames.gwars.Config;
import nl.tiesgames.gwars.Golems.GolemManager;
import nl.tiesgames.gwars.KitManager.KitManager;
import nl.tiesgames.gwars.Teams.Teams;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PlayerDeathEvent implements Listener {

	@EventHandler
	public void onRedPlayerRespawn(PlayerRespawnEvent e) {

		Player p = e.getPlayer();

		if (Teams.red.hasPlayer(p) && !GolemManager.redgolemdeath()) {

			e.setRespawnLocation(Config.getLocation("red"));
			KitManager.BasicKit(p);

		} else if(GolemManager.redgolemdeath()) {

			p.sendMessage(ChatColor.GOLD
					+ "Your teams golem is death! You cant respawn!");
			e.setRespawnLocation(Config.getLocation("spawn"));

		}

	}

	@EventHandler
	public void onYellowPlayerRespawn(PlayerRespawnEvent e) {

		Player p = e.getPlayer();

		if (Teams.yellow.hasPlayer(p) && !GolemManager.yellowgolemdeath()) {

			e.setRespawnLocation(Config.getLocation("yellow"));
			KitManager.BasicKit(p);

		} else if(GolemManager.yellowgolemdeath()) {

			p.sendMessage(ChatColor.GOLD
					+ "Your teams golem is death! You cant respawn!");
			e.setRespawnLocation(Config.getLocation("yellow"));

		}

	}

	@EventHandler
	public void onOrangePlayerRespawn(PlayerRespawnEvent e) {

		Player p = e.getPlayer();

		if (Teams.orange.hasPlayer(p) && !GolemManager.orangegolemdeath()) {

			e.setRespawnLocation(Config.getLocation("orange"));
			KitManager.BasicKit(p);

		} else if(GolemManager.orangegolemdeath()) {

			p.sendMessage(ChatColor.GOLD
					+ "Your teams golem is death! You cant respawn!");
			e.setRespawnLocation(Config.getLocation("spawn"));

		}
		
	}
		
		@EventHandler
		public void onCyanPlayerRespawn(PlayerRespawnEvent e) {

			Player p = e.getPlayer();

			if (Teams.cyan.hasPlayer(p) && !GolemManager.cyangolemdeath()) {

				e.setRespawnLocation(Config.getLocation("cyan"));
				KitManager.BasicKit(p);

			} else if(GolemManager.cyangolemdeath()) {

				p.sendMessage(ChatColor.GOLD
						+ "Your teams golem is death! You cant respawn!");
				e.setRespawnLocation(Config.getLocation("spawn"));

	}

}
		
		@EventHandler
		public void onPlayerRespawn(PlayerRespawnEvent e) {

			Player p = e.getPlayer();

			if (!Teams.red.hasPlayer(p) || !Teams.yellow.hasPlayer(p) || !Teams.orange.hasPlayer(p) || !Teams.cyan.hasPlayer(p)) {

				e.setRespawnLocation(Config.getLocation("spawn"));
		
}
			
		}
		
}
