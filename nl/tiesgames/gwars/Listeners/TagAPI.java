package nl.tiesgames.gwars.Listeners;

import nl.tiesgames.gwars.Teams.Teams;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.kitteh.tag.AsyncPlayerReceiveNameTagEvent;

public class TagAPI implements Listener {

	@EventHandler
	public void onPlayerReceiveNameTag(AsyncPlayerReceiveNameTagEvent e) {

		Player p = e.getNamedPlayer();

		if (Teams.red.hasPlayer(p)) {

			e.setTag(ChatColor.RED + p.getName());
			p.setPlayerListName(ChatColor.RED + p.getName());

		} else if (Teams.yellow.hasPlayer(p)) {

			e.setTag(ChatColor.YELLOW + p.getName());
			p.setPlayerListName(ChatColor.YELLOW + p.getName());

		} else if (Teams.orange.hasPlayer(p)) {

			e.setTag(ChatColor.GOLD + p.getName());
			p.setPlayerListName(ChatColor.GOLD + p.getName());

		} else if (Teams.cyan.hasPlayer(p)) {

			e.setTag(ChatColor.AQUA + p.getName());
			p.setPlayerListName(ChatColor.AQUA+ p.getName());

		}

	}

}
