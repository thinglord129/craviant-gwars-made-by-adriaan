package nl.tiesgames.gwars.Listeners;

import nl.tiesgames.gwars.Config;
import nl.tiesgames.gwars.Game.GameManager;
import nl.tiesgames.gwars.Teams.Teams;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

public class JoinLeave implements Listener {

	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent e) {
		if (GameManager.isStarted()) {
			e.disallow(Result.KICK_OTHER, ChatColor.GOLD
					+ "This game is already ingame. Join another game.");

		}

	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {

		Player p = e.getPlayer();

		e.setJoinMessage(null);
		
		p.sendMessage(ChatColor.RED
				+ "Welcome on the"
				+ ChatColor.DARK_GRAY
				+ " gWars "
				+ ChatColor.RED
				+ "closed beta! Please play fair in this beta, and keep in mind that not everything is working yet.");
		p.sendMessage(ChatColor.RED
				+ "If you found a bug, please report it to @AdrioGames or @AbalGames on Twitter.");
		p.sendMessage(ChatColor.RED + "Thanks for playing the beta.");
		p.teleport(Config.getLocation("spawn"));
		p.setGameMode(GameMode.ADVENTURE);
		p.setHealth(20);
		p.setFoodLevel(20);
		p.setLevel(0);
		p.getInventory().clear();
		p.getInventory().setArmorContents(null);
		
		Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective obj = board.registerNewObjective("countdown", "dummy");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		e.getPlayer().setScoreboard(board);

	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		
		Player p = e.getPlayer();

		e.setQuitMessage(null);
		Teams.leaveTeam(p);

	}

}
