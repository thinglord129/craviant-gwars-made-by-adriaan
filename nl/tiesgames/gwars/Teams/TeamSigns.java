package nl.tiesgames.gwars.Teams;

import nl.tiesgames.gwars.Config;
import nl.tiesgames.gwars.Game.GameManager;
import nl.tiesgames.gwars.Managers.SignManager;

import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.kitteh.tag.TagAPI;

public class TeamSigns implements Listener {

	@EventHandler
	public void SignPlace(SignChangeEvent e) {

		Player p = e.getPlayer();

		if (p.isOp()) {
			if (e.getLine(0).equalsIgnoreCase("[Red]")) {
				e.setLine(0, ChatColor.AQUA + "Team" + ChatColor.RED + " Red");
				e.setLine(1, ChatColor.WHITE + "Players " + ChatColor.GOLD
						+ Teams.red.getSize());
				e.setLine(2, ChatColor.AQUA + "Right click");
				e.setLine(3, ChatColor.AQUA + "to join!");
				Config.addSignLocation("red", e.getBlock().getLocation());
			} else if (e.getLine(0).equalsIgnoreCase("[Yellow]")) {
				e.setLine(0, ChatColor.AQUA + "Team" + ChatColor.YELLOW
						+ " Yellow");
				e.setLine(1, ChatColor.WHITE + "Players " + ChatColor.GOLD
						+ Teams.yellow.getSize());
				e.setLine(2, ChatColor.AQUA + "Right click");
				e.setLine(3, ChatColor.AQUA + "to join!");
				Config.addSignLocation("yellow", e.getBlock().getLocation());
			} else if (e.getLine(0).equalsIgnoreCase("[Orange]")) {
				e.setLine(0, ChatColor.AQUA + "Team" + ChatColor.GOLD
						+ " Orange");
				e.setLine(1, ChatColor.WHITE + "Players " + ChatColor.GOLD
						+ Teams.orange.getSize());
				e.setLine(2, ChatColor.AQUA + "Right click");
				e.setLine(3, ChatColor.AQUA + "to join!");
				Config.addSignLocation("orange", e.getBlock().getLocation());
			} else if (e.getLine(0).equalsIgnoreCase("[Cyan]")) {
				e.setLine(0, ChatColor.AQUA + "Team" + ChatColor.AQUA + " Cyan");
				e.setLine(1, ChatColor.WHITE + "Players " + ChatColor.GOLD
						+ Teams.cyan.getSize());
				e.setLine(2, ChatColor.AQUA + "Right click");
				e.setLine(3, ChatColor.AQUA + "to join!");
				Config.addSignLocation("cyan", e.getBlock().getLocation());
			} else {
				return;
			}
		}
	}

	@EventHandler
	public void onPlayerSign(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK)
			if (e.getClickedBlock().getState() instanceof Sign) {

				Sign s = (Sign) e.getClickedBlock().getState();
				if (s.getLine(0).equalsIgnoreCase(ChatColor.AQUA + "Team" + ChatColor.RED + " Red")) {

					Teams.join(Teams.red, p);
					if (Teams.red.hasPlayer(p)) {
						TagAPI.refreshPlayer(p);
						p.setPlayerListName(ChatColor.RED + p.getName());
					}
					
					if (GameManager.isStarted()) {
						p.teleport(Config.getLocation("red"));
					}
					
					SignManager.updateRedSigns();
				} else if (s.getLine(0).equalsIgnoreCase(ChatColor.AQUA + "Team" + ChatColor.YELLOW + " Yellow")) {

					Teams.join(Teams.yellow, p);
					if (Teams.yellow.hasPlayer(p)) {
						TagAPI.refreshPlayer(p);
						p.setPlayerListName(ChatColor.YELLOW + p.getName());
					}
					
					if (GameManager.isStarted()) {
						p.teleport(Config.getLocation("yellow"));
					}
					
					SignManager.updateYellowSigns();
				} else if (s.getLine(0).equalsIgnoreCase(ChatColor.AQUA + "Team" + ChatColor.GOLD + " Orange")) {

					Teams.join(Teams.orange, p);
					if (Teams.orange.hasPlayer(p)) {
						TagAPI.refreshPlayer(p);
						p.setPlayerListName(ChatColor.GOLD + p.getName());
					}
					
					if (GameManager.isStarted()) {
						p.teleport(Config.getLocation("orange"));
					}
					
					SignManager.updateOrangeSigns();
				} else if (s.getLine(0).equalsIgnoreCase(ChatColor.AQUA + "Team" + ChatColor.AQUA + " Cyan")) {

					Teams.join(Teams.cyan, p);
					if (Teams.cyan.hasPlayer(p)) {
						TagAPI.refreshPlayer(p);
						p.setPlayerListName(ChatColor.AQUA + p.getName());
					}
					
					if (GameManager.isStarted()) {
						p.teleport(Config.getLocation("cyan"));
					}
					
					SignManager.updateCyanSigns();
				}
			}
	}
}
