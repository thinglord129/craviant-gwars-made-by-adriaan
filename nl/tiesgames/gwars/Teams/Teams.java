package nl.tiesgames.gwars.Teams;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class Teams {

	public static Scoreboard board = Bukkit.getScoreboardManager()
			.getNewScoreboard();

	public static Team red = board.registerNewTeam(ChatColor.RED + "Red");
	public static Team yellow = board.registerNewTeam(ChatColor.YELLOW
			+ "Yellow");
	public static Team orange = board
			.registerNewTeam(ChatColor.GOLD + "Orange");
	public static Team cyan = board.registerNewTeam(ChatColor.AQUA + "Cyan");

	public static void join(Team t, Player p) {

		if (!inTeam(p) && t.getSize() < 15) {
			
			t.addPlayer(p);
			p.sendMessage(ChatColor.DARK_PURPLE + "You joined team "
					+ t.getDisplayName() + ChatColor.DARK_PURPLE + "!");

		} else if (inTeam(p)) {

			p.sendMessage(ChatColor.RED + "You are already in a team");

		} else if (t.getSize() == 15) {

			p.sendMessage(ChatColor.DARK_PURPLE + "Team " + t.getDisplayName()
					+ ChatColor.DARK_PURPLE + " is full!");

		}

	}

	public static Team getTeam(Player p) {
		if (red.hasPlayer(p)) {
			return red;
		} else if (yellow.hasPlayer(p)) {
			return yellow;
		} else if (orange.hasPlayer(p)) {
			return orange;
		} else if (cyan.hasPlayer(p)) {
			return cyan;
		} else {
			return null;
		}
	}

	public static Team leaveTeam(Player p) {

		if (red.hasPlayer(p)) {

			red.removePlayer(p);

			return red;

		} else if (yellow.hasPlayer(p)) {

			yellow.removePlayer(p);

			return yellow;

		} else if (orange.hasPlayer(p)) {

			orange.removePlayer(p);

			return orange;

		} else if (cyan.hasPlayer(p)) {

			cyan.removePlayer(p);

			return cyan;

		} else {

			return null;

		}

	}

	public static boolean inTeam(Player p) {

		if (red.hasPlayer(p)) {

			return true;

		} else if (yellow.hasPlayer(p)) {

			return true;

		} else if (orange.hasPlayer(p)) {

			return true;

		} else if (cyan.hasPlayer(p)) {

			return true;

		} else {

			return false;

		}

	}

}
