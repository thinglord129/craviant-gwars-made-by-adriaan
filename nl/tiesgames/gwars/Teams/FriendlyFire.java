package nl.tiesgames.gwars.Teams;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class FriendlyFire implements Listener {

	@EventHandler
	public void onRedFriendlyFire(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
			Player ent = (Player) e.getEntity();
			Player damager = (Player) e.getDamager();

			if (Teams.getTeam(ent).equals(Teams.getTeam(damager))) {
				e.setCancelled(true);
			}
		}
	}
}
