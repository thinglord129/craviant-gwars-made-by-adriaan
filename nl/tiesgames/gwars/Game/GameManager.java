package nl.tiesgames.gwars.Game;

public class GameManager {

	private static boolean started = false;

	public static boolean isStarted() {

		return started;

	}

	public static void setStarted(boolean b) {
		started = b;
	}

}
