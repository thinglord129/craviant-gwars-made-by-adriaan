package nl.tiesgames.gwars.Game;

import nl.tiesgames.gwars.Config;
import nl.tiesgames.gwars.KitManager.KitManager;
import nl.tiesgames.gwars.Managers.ScoreboardManager;
import nl.tiesgames.gwars.Teams.Teams;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;

public class Game implements Runnable {

	public static int seconds = 121;
	public static boolean forcestart = false;

	@Override
	public void run() {

		if (seconds != 0) {
			seconds--;

			for (Player p : Bukkit.getOnlinePlayers()) {
				ScoreboardManager.updateScoreboard(p);
			}

		} else if (seconds == 0 && Bukkit.getOnlinePlayers().length == 60 || forcestart) {
			

			Bukkit.broadcastMessage(ChatColor.GRAY + "-=-=-=-=- " + ChatColor.GOLD + "GWARS" + ChatColor.GRAY + " -=-=-=-=-");
			Bukkit.broadcastMessage(ChatColor.GOLD + "            Game Started! Good Luck!");
			Bukkit.broadcastMessage(ChatColor.GRAY + "-=-=-=-=- " + ChatColor.GOLD + "GWARS" + ChatColor.GRAY + " -=-=-=-=-");
			GameManager.setStarted(true);

			for (Player p : Bukkit.getOnlinePlayers()) {
				p.getInventory().clear();
				p.getScoreboard().clearSlot(DisplaySlot.SIDEBAR);

				Bukkit.getScheduler().cancelAllTasks();

				if (Teams.red.hasPlayer(p)) {
					p.teleport(Config.getLocation("red"));
					KitManager.BasicKit(p);

				} else if (Teams.yellow.hasPlayer(p)) {
					p.teleport(Config.getLocation("yellow"));
					KitManager.BasicKit(p);

				} else if (Teams.orange.hasPlayer(p)) {
					p.teleport(Config.getLocation("orange"));
					KitManager.BasicKit(p);

				} else if (Teams.cyan.hasPlayer(p)) {
					p.teleport(Config.getLocation("cyan"));
					KitManager.BasicKit(p);

				}

			}

		} else {

			seconds = 121;
			Bukkit.broadcastMessage(ChatColor.GOLD
					+ "Not enough players! Resetting Timer!");

		}

	}

}
