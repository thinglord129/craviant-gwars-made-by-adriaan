package nl.tiesgames.gwars;

import java.util.logging.Logger;

import nl.tiesgames.gwars.BlockRespawn.Coal;
import nl.tiesgames.gwars.BlockRespawn.Diamond;
import nl.tiesgames.gwars.BlockRespawn.Emerald;
import nl.tiesgames.gwars.BlockRespawn.Gold;
import nl.tiesgames.gwars.BlockRespawn.Iron;
import nl.tiesgames.gwars.BlockRespawn.Leaves;
import nl.tiesgames.gwars.BlockRespawn.Melon;
import nl.tiesgames.gwars.BlockRespawn.Oak;
import nl.tiesgames.gwars.BlockRespawn.PlayerPlaceBlock;
import nl.tiesgames.gwars.BlockRespawn.Redstone;
import nl.tiesgames.gwars.Commands.Commands;
import nl.tiesgames.gwars.Game.Game;
import nl.tiesgames.gwars.Golems.GolemDamage;
import nl.tiesgames.gwars.Golems.GolemDeath;
import nl.tiesgames.gwars.Listeners.ChatEvent;
import nl.tiesgames.gwars.Listeners.DeathMessage;
import nl.tiesgames.gwars.Listeners.JoinLeave;
import nl.tiesgames.gwars.Listeners.PlayerDeathEvent;
import nl.tiesgames.gwars.Listeners.TagAPI;
import nl.tiesgames.gwars.Nexus.CyanNexus;
import nl.tiesgames.gwars.Nexus.OrangeNexus;
import nl.tiesgames.gwars.Nexus.RedNexus;
import nl.tiesgames.gwars.Nexus.YellowNexus;
import nl.tiesgames.gwars.Teams.FriendlyFire;
import nl.tiesgames.gwars.Teams.TeamSigns;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	Logger log = Logger.getLogger("Minecraft"); 
	
	static Plugin plugin;
			
	@Override
	public void onEnable() {
		log.info("[gWars] " + this.getDescription().getVersion() + " is enabled");
		Main.plugin = this;
		
		Config.initConfig();
		Config.deserializeSignLocations("yellow");
		Config.deserializeSignLocations("red");
		Config.deserializeSignLocations("cyan");
		Config.deserializeSignLocations("orange");
		
		getCommand("forcestart").setExecutor(new Commands());
		getCommand("gwars").setExecutor(new Commands());
		getCommand("setspawn").setExecutor(new Commands());
		getCommand("redspawn").setExecutor(new Commands());
		getCommand("yellowspawn").setExecutor(new Commands());
		getCommand("orangespawn").setExecutor(new Commands());
		getCommand("cyanspawn").setExecutor(new Commands());
		getCommand("redgolem").setExecutor(new Commands());
		getCommand("yellowgolem").setExecutor(new Commands());
		getCommand("orangegolem").setExecutor(new Commands());
		getCommand("cyangolem").setExecutor(new Commands());
	
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new JoinLeave(), this);
		pm.registerEvents(new ChatEvent(), this);
		pm.registerEvents(new PlayerDeathEvent(), this);
		pm.registerEvents(new DeathMessage(), this);
		pm.registerEvents(new TeamSigns(), this);
		pm.registerEvents(new FriendlyFire(), this);
		pm.registerEvents(new GolemDamage(), this);
		pm.registerEvents(new GolemDeath(), this);
		pm.registerEvents(new PlayerPlaceBlock(), this);
		pm.registerEvents(new TagAPI(), this);
		pm.registerEvents(new RedNexus(), this);
		pm.registerEvents(new OrangeNexus(), this);
		pm.registerEvents(new YellowNexus(), this);
		pm.registerEvents(new CyanNexus(), this);
		pm.registerEvents(new Coal(), this);
		pm.registerEvents(new Iron(), this);
		pm.registerEvents(new Gold(), this);
		pm.registerEvents(new Diamond(), this);
		pm.registerEvents(new Emerald(), this);
		pm.registerEvents(new Redstone(), this);
		pm.registerEvents(new Melon(), this);
		pm.registerEvents(new Oak(), this);
		pm.registerEvents(new Leaves(), this);
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(getMain(), new Game(), 0L, 20L);
	
	}
	
	@Override
	public void onDisable() {
		log.info("[gWars] " + this.getDescription().getVersion() + " is disabled!");

	}
	
	public static Plugin getMain(){
		return plugin;
		
	}

}
