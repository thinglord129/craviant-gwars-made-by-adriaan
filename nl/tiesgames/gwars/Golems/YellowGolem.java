package nl.tiesgames.gwars.Golems;

import nl.tiesgames.gwars.Config;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Golem;

public class YellowGolem {

	public static void spawn() {

		Golem golem = (Golem) Bukkit.getWorld("world").spawnEntity(
				Config.getLocation("yellowgolem"), EntityType.IRON_GOLEM);

		golem.setCustomName(ChatColor.YELLOW + "Yellow Golem");
		golem.setMaxHealth(500.0);
		golem.setHealth(500.0);
		golem.setRemoveWhenFarAway(false);

	}

}
