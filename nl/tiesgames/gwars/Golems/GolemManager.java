package nl.tiesgames.gwars.Golems;

public class GolemManager {

	private static boolean redgolemdeath = false;
	private static boolean yellowgolemdeath = false;
	private static boolean orangeolemdeath = false;
	private static boolean cyangolemdeath = false;

	public static boolean redgolemdeath() {

		return redgolemdeath;

	}

	public static void setredgolemdeath(boolean b) {
		redgolemdeath = b;

	}

	public static boolean yellowgolemdeath() {

		return yellowgolemdeath;

	}

	public static void setyellowgolemdeath(boolean b) {
		yellowgolemdeath = b;

	}

	public static boolean orangegolemdeath() {

		return orangeolemdeath;

	}

	public static void setorangegolemdeath(boolean b) {
		orangeolemdeath = b;

	}

	public static boolean cyangolemdeath() {

		return cyangolemdeath;

	}

	public static void setcyangolemdeath(boolean b) {
		cyangolemdeath = b;

	}

}
