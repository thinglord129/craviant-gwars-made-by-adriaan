package nl.tiesgames.gwars.Golems;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class GolemDeath implements Listener {

	@EventHandler
	public void onRedGolemDeath(EntityDeathEvent e) {

		if ((e.getEntity() instanceof LivingEntity)) {

			LivingEntity ent = (LivingEntity) e.getEntity();

			if ((ent.getCustomName().contains(ChatColor.RED + "Red Golem"))
					&& GolemManager.orangegolemdeath()
					&& GolemManager.cyangolemdeath()) {
						
						for (Player p : Bukkit.getOnlinePlayers()) {

							p.kickPlayer(ChatColor.RED + "The red golem is death! " + ChatColor.YELLOW
									+ "Team Yellow has won the game!");
							Bukkit.getServer().shutdown();
							
						}

			} else if ((ent.getCustomName().contains(ChatColor.RED
					+ "Red Golem"))
					&& GolemManager.yellowgolemdeath()
					&& GolemManager.cyangolemdeath()) {
						
						for (Player p : Bukkit.getOnlinePlayers()) {

							p.kickPlayer(ChatColor.RED + "The red golem is death! " + ChatColor.GOLD + "Team Orange has won the game!");
							Bukkit.getServer().shutdown();
						
						}
				
				} else if ((ent.getCustomName().contains(ChatColor.RED
						+ "Red Golem"))
						&& GolemManager.yellowgolemdeath()
						&& GolemManager.orangegolemdeath()) {
					Bukkit.getServer().shutdown();
							
							for (Player p : Bukkit.getOnlinePlayers()) {

								p.kickPlayer(ChatColor.RED + "The red golem is death! " + ChatColor.AQUA
										+ "Team Cyan has won the game!");
								Bukkit.getServer().shutdown();
							
						}

				} else if ((ent.getCustomName().contains(ChatColor.RED
						+ "Red Golem"))
						&& GolemManager.orangegolemdeath()
						&& GolemManager.cyangolemdeath()) {

					GolemManager.setredgolemdeath(true);

					Bukkit.broadcastMessage(ChatColor.RED
							+ "The red golem is death! The red team is down!");

					for (Player p : Bukkit.getOnlinePlayers()) {
						p.playSound(p.getLocation(), Sound.ENDERDRAGON_GROWL,
								10.0F, 1.0F);

					}

				}

			}

		}

	@EventHandler
	public void onYellowGolemDeath(EntityDeathEvent e) {

		if ((e.getEntity() instanceof LivingEntity)) {

			LivingEntity ent = (LivingEntity) e.getEntity();

			if (ent.getCustomName().contains(ChatColor.YELLOW + "Yellow Golem")
					&& GolemManager.orangegolemdeath()
					&& GolemManager.cyangolemdeath()) {
				
				for (Player p : Bukkit.getOnlinePlayers()) {

					p.kickPlayer(ChatColor.YELLOW + "The YELLOW golem is death! " + ChatColor.RED + "Team Red won the game!");
					Bukkit.getServer().shutdown();

				}

			} else if (ent.getCustomName().contains(
					ChatColor.YELLOW + "Yellow Golem")
					&& GolemManager.redgolemdeath()
					&& GolemManager.cyangolemdeath()) {

				for (Player p : Bukkit.getOnlinePlayers()) {

					p.kickPlayer(ChatColor.YELLOW + "The YELLOW golem is death! " + ChatColor.GOLD + "Team Orange won the game!");
					Bukkit.getServer().shutdown();

				}

			} else if (ent.getCustomName().contains(
					ChatColor.YELLOW + "Yellow Golem")
					&& GolemManager.redgolemdeath()
					&& GolemManager.orangegolemdeath()) {

				for (Player p : Bukkit.getOnlinePlayers()) {

					p.kickPlayer(ChatColor.YELLOW + "The YELLOW golem is death! " +ChatColor.AQUA + "Team Cyan won the game!");
					Bukkit.getServer().shutdown();
					
				}

				}else if(ent.getCustomName().contains(
						ChatColor.YELLOW + "Yellow Golem")) {
					Bukkit.getServer().reload();

				GolemManager.setyellowgolemdeath(true);

				Bukkit.broadcastMessage(ChatColor.YELLOW
						+ "The yellow golem is death! The yellow team is down!");

				for (Player p : Bukkit.getOnlinePlayers()) {
					p.playSound(p.getLocation(), Sound.ENDERDRAGON_GROWL,
							10.0F, 1.0F);

				}

			}

		}

	}

	@EventHandler
	public void onOrangeGolemDeath(EntityDeathEvent e) {

		if ((e.getEntity() instanceof LivingEntity)) {

			LivingEntity ent = (LivingEntity) e.getEntity();
		   
			if (ent.getCustomName().contains(ChatColor.GOLD + "Orange Golem")
					&& GolemManager.yellowgolemdeath()
					&& GolemManager.cyangolemdeath()) {
				
				for (Player p : Bukkit.getOnlinePlayers()) {

					p.kickPlayer(ChatColor.RED + "Team Red won the game!");
					Bukkit.getServer().shutdown();
					
				}
				
			}else if(ent.getCustomName().contains(ChatColor.GOLD + "Orange Golem") && GolemManager.redgolemdeath() && GolemManager.cyangolemdeath()) {
				
				for (Player p : Bukkit.getOnlinePlayers()) {

					p.kickPlayer(ChatColor.YELLOW+ "Team Yellow won the game!");
					Bukkit.getServer().shutdown();
					
				}
				
			}else if(ent.getCustomName().contains(ChatColor.GOLD + "Orange Golem") && GolemManager.redgolemdeath() && GolemManager.yellowgolemdeath()) {
				
				for (Player p : Bukkit.getOnlinePlayers()) {

					p.kickPlayer(ChatColor.AQUA + "Team Cyan won the game!");
					Bukkit.getServer().shutdown();
					
				}

			}else if (ent.getCustomName().contains(ChatColor.GOLD + "Orange Golem")) {

				GolemManager.setorangegolemdeath(true);

				Bukkit.broadcastMessage(ChatColor.GOLD
						+ "The orange golem is death! The orange team is down!");

				for (Player p : Bukkit.getOnlinePlayers()) {
					p.playSound(p.getLocation(), Sound.ENDERDRAGON_GROWL,
							10.0F, 1.0F);

				}

			}

		}
			
		}

	@EventHandler
	public void onCyanGolemDeath(EntityDeathEvent e) {

		if ((e.getEntity() instanceof LivingEntity)) {

			LivingEntity ent = (LivingEntity) e.getEntity();
			
			if(ent.getCustomName().contains(ChatColor.AQUA + "Cyan Golem") && GolemManager.yellowgolemdeath() && GolemManager.orangegolemdeath()) {
				
				for (Player p : Bukkit.getOnlinePlayers()) {

					p.kickPlayer(ChatColor.AQUA + "The CYAN golem is death! " + ChatColor.RED + "Team Red won the game!");
					Bukkit.getServer().shutdown();
					
				}
				
			}else if(ent.getCustomName().contains(ChatColor.AQUA + "Cyan Golem") && GolemManager.yellowgolemdeath() && GolemManager.orangegolemdeath()) {
				
				for (Player p : Bukkit.getOnlinePlayers()) {

					p.kickPlayer(ChatColor.AQUA + "The CYAN golem is death! " + ChatColor.YELLOW + "Team Yellow won the game!");
					Bukkit.getServer().shutdown();
					
				}
				
			}else if(ent.getCustomName().contains(ChatColor.AQUA + "Cyan Golem") && GolemManager.redgolemdeath()&& GolemManager.yellowgolemdeath()) {
				
				for (Player p : Bukkit.getOnlinePlayers()) {

					p.kickPlayer(ChatColor.AQUA + "The CYAN golem is death! " + ChatColor.YELLOW + "Team Orange won the game!");
					Bukkit.getServer().shutdown();
					
				}

			}else if (ent.getCustomName().contains(ChatColor.AQUA + "Cyan Golem")) {

				GolemManager.setcyangolemdeath(true);

				Bukkit.broadcastMessage(ChatColor.AQUA
						+ "The cyan golem is death! The cyan team is down!");

				for (Player p : Bukkit.getOnlinePlayers()) {
					p.playSound(p.getLocation(), Sound.ENDERDRAGON_GROWL,
							10.0F, 1.0F);

				}

			}

		}

	}

}
