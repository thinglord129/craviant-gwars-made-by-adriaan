package nl.tiesgames.gwars.Golems;

import nl.tiesgames.gwars.Config;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Golem;

public class CyanGolem {
	
	@SuppressWarnings("deprecation")
	public static void spawn() {
		
		Golem golem = (Golem) Bukkit.getWorld("world").spawnEntity(Config.getLocation("cyangolem"), EntityType.IRON_GOLEM);
		
		golem.setCustomName(ChatColor.AQUA + "Cyan Golem");
		golem.setMaxHealth(500);
		golem.setHealth(500);
		golem.setRemoveWhenFarAway(false);

}
	
}
