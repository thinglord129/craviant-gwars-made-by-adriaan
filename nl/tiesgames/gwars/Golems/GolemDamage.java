package nl.tiesgames.gwars.Golems;

import nl.tiesgames.gwars.Teams.Teams;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class GolemDamage implements Listener {

	@EventHandler
	public void onRedGolemDamage(EntityDamageByEntityEvent e) {

		if ((e.getEntity() instanceof LivingEntity)) {

			LivingEntity ent = (LivingEntity) e.getEntity();
			Entity damager = e.getDamager();

			if (damager instanceof Player) {

				if (ent.getCustomName().contains(ChatColor.RED + "Red Golem")) {
					if (Teams.red.hasPlayer((OfflinePlayer) damager)) {

						((Player) damager).sendMessage(ChatColor.GOLD
								+ "You cant hit your own golem!");

						e.setCancelled(true);

					}

				}

			}

		}

	}

	@EventHandler
	public void onYellowGolemDamage(EntityDamageByEntityEvent e) {

		if ((e.getEntity() instanceof LivingEntity)) {

			LivingEntity ent = (LivingEntity) e.getEntity();
			Entity damager = e.getDamager();

			if (damager instanceof Player) {

				if (ent.getCustomName().contains(
						ChatColor.YELLOW + "Yellow Golem")) {
					if (Teams.yellow.hasPlayer((OfflinePlayer) damager)) {

						((Player) damager).sendMessage(ChatColor.GOLD
								+ "You cant hit your own golem!");

						e.setCancelled(true);

					}

				}

			}

		}

	}

	@EventHandler
	public void onOrangeGolemDamage(EntityDamageByEntityEvent e) {

		if ((e.getEntity() instanceof LivingEntity)) {

			LivingEntity ent = (LivingEntity) e.getEntity();
			Entity damager = e.getDamager();

			if (damager instanceof Player) {

				if (ent.getCustomName().contains(
						ChatColor.GOLD + "Orange Golem")) {
					if (Teams.orange.hasPlayer((OfflinePlayer) damager)) {

						((Player) damager).sendMessage(ChatColor.GOLD
								+ "You cant hit your own golem!");

						e.setCancelled(true);

					}

				}

			}

		}

	}

	@EventHandler
	public void onCyanGolemDamage(EntityDamageByEntityEvent e) {

		if ((e.getEntity() instanceof LivingEntity)) {

			LivingEntity ent = (LivingEntity) e.getEntity();
			Entity damager = e.getDamager();

			if (damager instanceof Player) {

				if (ent.getCustomName().contains(ChatColor.AQUA + "Cyan Golem")) {
					if (Teams.cyan.hasPlayer((OfflinePlayer) damager)) {

						((Player) damager).sendMessage(ChatColor.GOLD
								+ "You cant hit your own golem!");

						e.setCancelled(true);

					}

				}

			}

		}

	}

}