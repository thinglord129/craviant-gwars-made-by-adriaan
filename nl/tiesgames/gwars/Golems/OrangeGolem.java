package nl.tiesgames.gwars.Golems;

import nl.tiesgames.gwars.Config;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Golem;

public class OrangeGolem {
	
	@SuppressWarnings("deprecation")
	public static void spawn() {
		
		Golem golem = (Golem) Bukkit.getWorld("world").spawnEntity(Config.getLocation("orangegolem"), EntityType.IRON_GOLEM);
		
		golem.setCustomName(ChatColor.GOLD + "Orange Golem");
		golem.setMaxHealth(500);
		golem.setHealth(500);
		golem.setRemoveWhenFarAway(false);

}
	
}
