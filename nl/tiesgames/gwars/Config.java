package nl.tiesgames.gwars;

import java.util.ArrayList;

import nl.tiesgames.gwars.Managers.SignManager;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class Config {

	static FileConfiguration conf = Main.getMain().getConfig();

	public static void initConfig() {
		if (!conf.contains("teamsigns.red")) {
			conf.set("teamsigns.red", new ArrayList<String>());
		}

		if (!conf.contains("teamsigns.cyan")) {
			conf.set("teamsigns.cyan", new ArrayList<String>());
		}

		if (!conf.contains("teamsigns.yellow")) {
			conf.set("teamsigns.yellow", new ArrayList<String>());
		}

		if (!conf.contains("teamsigns.orange")) {
			conf.set("teamsigns.orange", new ArrayList<String>());
		}

	}

	public static void setLocation(Player p, String locstring) {
		if (!conf.contains("location." + locstring)) {
			conf.addDefault("location." + locstring + ".world", p.getLocation()
					.getWorld().getName());
			conf.addDefault("location." + locstring + ".x", p.getLocation()
					.getBlockX());
			conf.addDefault("location." + locstring + ".y", p.getLocation()
					.getBlockY());
			conf.addDefault("location." + locstring + ".z", p.getLocation()
					.getBlockZ());
			conf.addDefault("location." + locstring + ".pitch", p.getLocation()
					.getPitch());
			conf.addDefault("location." + locstring + ".pitch", p.getLocation()
					.getYaw());
			conf.options().copyDefaults(true);
			Main.getMain().saveConfig();

			return;

		} else {

			conf.set("location." + locstring + ".world", p.getLocation()
					.getWorld().getName());
			conf.set("location." + locstring + ".x", p.getLocation()
					.getBlockX());
			conf.set("location." + locstring + ".y", p.getLocation()
					.getBlockY());
			conf.set("location." + locstring + ".z", p.getLocation()
					.getBlockZ());
			conf.set("location." + locstring + ".pitch", p.getLocation()
					.getPitch());
			conf.set("location." + locstring + ".pitch", p.getLocation()
					.getYaw());

			Main.getMain().saveConfig();

			return;
		}

	}

	public static Location getLocation(String locstring) {
		return new Location(Bukkit.getWorld(conf.getString("location."
				+ locstring + ".world")), conf.getInt("location." + locstring
				+ ".x"), conf.getInt("location." + locstring + ".y"),
				conf.getInt("location." + locstring + ".z"),
				(float) conf.getDouble("location." + locstring + ".pitch"),
				(float) conf.getDouble("location." + locstring + ".yaw"));

	}

	@SuppressWarnings("unchecked")
	public static void addSignLocation(String team, Location loc) {
		ArrayList<String> list = (ArrayList<String>) conf.getList("teamsigns."
				+ team);
		list.add(loc.getBlockX() + "," + loc.getBlockY() + ","
				+ loc.getBlockZ());
		conf.set("teamsigns." + team, list);

		if (team.equalsIgnoreCase("red")) {
			SignManager.redsigns.add(loc);
		} else if (team.equalsIgnoreCase("cyan")) {
			SignManager.cyansigns.add(loc);
		} else if (team.equalsIgnoreCase("yellow")) {
			SignManager.yellowsigns.add(loc);
		} else if (team.equalsIgnoreCase("orange")) {
			SignManager.orangesigns.add(loc);
		} else {
			return;
		}

		Main.getMain().saveConfig();
	}

	@SuppressWarnings("unchecked")
	public static void deserializeSignLocations(String team) {
		ArrayList<String> list = (ArrayList<String>) conf.getList("teamsigns."
				+ team);
		for (String locstring : list) {
			String[] locsdata = locstring.split(",");
			Location newlocation = new Location(Bukkit.getWorld("world"),
					Integer.parseInt(locsdata[0]),
					Integer.parseInt(locsdata[1]),
					Integer.parseInt(locsdata[2]));

			if (team.equalsIgnoreCase("red")) {
				SignManager.redsigns.add(newlocation);
			} else if (team.equalsIgnoreCase("cyan")) {
				SignManager.cyansigns.add(newlocation);
			} else if (team.equalsIgnoreCase("yellow")) {
				SignManager.yellowsigns.add(newlocation);
			} else if (team.equalsIgnoreCase("orange")) {
				SignManager.orangesigns.add(newlocation);
			} else {
				return;
			}
		}
	}

}
