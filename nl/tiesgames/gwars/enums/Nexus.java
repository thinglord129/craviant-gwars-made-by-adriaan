package nl.tiesgames.gwars.enums;

public enum Nexus { CYAN("Cyan"), ORANGE("Orange"), RED("Red"), YELLOW("Yellow");

	String name;
	
	private Nexus(String namestring) {
		name = namestring;
	}
	
	public String getName(){
		return name;
	}

}
