package nl.tiesgames.gwars.Commands;

import nl.tiesgames.gwars.Config;
import nl.tiesgames.gwars.Game.Game;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Commands implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2,
			String[] args) {

		if (cmd.getName().equalsIgnoreCase("gwars")) {

			Player p = (Player) sender;

			p.sendMessage(ChatColor.RED + "gWars is developed by "
					+ ChatColor.WHITE + "GamingAdrio" + ChatColor.RED + ".");

		}

		if (cmd.getName().equalsIgnoreCase("setspawn")) {

			Player p = (Player) sender;

			if (p.isOp()) {

				Config.setLocation(p, "spawn");
				p.sendMessage(ChatColor.RED + "You have set the default spawn!");

			} else if (!p.isOp()) {

				p.sendMessage(ChatColor.RED
						+ "You dont have permission to use this command!");

			}

		}

		if (cmd.getName().equalsIgnoreCase("forcestart")) {
			Player p = (Player) sender;
			if (p.isOp()) {
				Game.forcestart = true;
				Game.seconds = 10;
				p.sendMessage(ChatColor.RED + "Forcestart active!");
			}
		}

		if (cmd.getName().equalsIgnoreCase("redspawn")) {

			Player p = (Player) sender;

			if (p.isOp()) {

				Config.setLocation(p, "red");
				p.sendMessage(ChatColor.RED + "You have set the spawn!");

			} else if (!p.isOp()) {

				p.sendMessage(ChatColor.RED
						+ "You dont have permission to use this command!");

			}

		}

		if (cmd.getName().equalsIgnoreCase("yellowspawn")) {

			Player p = (Player) sender;

			if (p.isOp()) {

				Config.setLocation(p, "yellow");
				p.sendMessage(ChatColor.RED + "You have set the spawn!");

			} else if (!p.isOp()) {

				p.sendMessage(ChatColor.RED
						+ "You dont have permission to use this command!");

			}

		}

		if (cmd.getName().equalsIgnoreCase("orangespawn")) {

			Player p = (Player) sender;

			if (p.isOp()) {

				Config.setLocation(p, "orange");
				p.sendMessage(ChatColor.RED + "You have set the spawn!");

			} else if (!p.isOp()) {

				p.sendMessage(ChatColor.RED
						+ "You dont have permission to use this command!");

			}

		}

		if (cmd.getName().equalsIgnoreCase("cyanspawn")) {

			Player p = (Player) sender;

			if (p.isOp()) {

				Config.setLocation(p, "cyan");
				p.sendMessage(ChatColor.RED + "You have set the spawn!");

			} else if (!p.isOp()) {

				p.sendMessage(ChatColor.RED
						+ "You dont have permission to use this command!");

			}

		}

		if (cmd.getName().equalsIgnoreCase("redgolem")) {

			Player p = (Player) sender;

			if (p.isOp()) {

				Config.setLocation(p, "redgolem");
				p.sendMessage(ChatColor.RED + "You have set the spawn!");

			} else if (!p.isOp()) {

				p.sendMessage(ChatColor.RED
						+ "You dont have permission to use this command!");

			}

		}

		if (cmd.getName().equalsIgnoreCase("yellowgolem")) {

			Player p = (Player) sender;

			if (p.isOp()) {

				Config.setLocation(p, "yellowgolem");
				p.sendMessage(ChatColor.RED + "You have set the spawn!");

			} else if (!p.isOp()) {

				p.sendMessage(ChatColor.RED
						+ "You dont have permission to use this command!");

			}

		}

		if (cmd.getName().equalsIgnoreCase("orangegolem")) {

			Player p = (Player) sender;

			if (p.isOp()) {

				Config.setLocation(p, "orangegolem");
				p.sendMessage(ChatColor.RED + "You have set the spawn!");

			} else if (!p.isOp()) {

				p.sendMessage(ChatColor.RED
						+ "You dont have permission to use this command!");

			}

		}

		if (cmd.getName().equalsIgnoreCase("cyangolem")) {

			Player p = (Player) sender;

			if (p.isOp()) {

				Config.setLocation(p, "cyangolem");
				p.sendMessage(ChatColor.RED + "You have set the spawn!");

			} else if (!p.isOp()) {

				p.sendMessage(ChatColor.RED
						+ "You dont have permission to use this command!");

			}

		}

		return false;

	}

}
